import styled from "styled-components"

export const Container = styled.div`
    position: relative;
    
    @media (min-width: 1000px) {
        position: fixed;
        width: 300px;
    }
`;

export const Item = styled.div`
    display: grid;
    grid-gap: 10px;
    padding: 20px 0;

    &:not(:last-of-type) {
        border-bottom: 1px solid #0b1e62;
    }
`;

export const Date = styled.time`
    font-size: 12px;
    font-weight: 600;
    display: flex;
    align-items: center;
    color: #7c98f9;

    svg {
        margin-right: 6px;
    }
`;

export const Name = styled.h3`
    font-size: 14px;
`;

export const Body = styled.p`
    font-size: 14px;
`;