// Hooks
import {useEffect, useState} from "react"

// Styles
import {
    Container,
    Item,
    Date,
    Name,
    Body
} from "./Widget.styles"

// Components
import Loader from "../Loader/Loader"
import BlockTitle from "../BlockTitle/BlockTitle"

// Icons
import CalendarIcon from "../../icons/Calendar"

// Serializers
import PostsSerializer from "../../serializers/posts.serializer"

const Widget = ({title, limit = 5, updateInt = 5}) => {
    const [posts, setPosts] = useState([])

    const getPosts = async () => {
        try {
            const response = await fetch("https://api.massrelevance.com/MassRelDemo/kindle.json")
            const data = await response.json()
            const posts = PostsSerializer(data)

            setPosts(posts)
        } catch (error) {
            console.error("Failed to retrieve data from Massrelevance API");
        }
    }

    useEffect(() => {
        getPosts()

        const updateInterval = setInterval(() => {
            setPosts([])
            getPosts()
        }, updateInt*60000)

        return () => {
            setPosts([])
            clearInterval(updateInterval);
        }
    }, [updateInt])

    return (
        <Container>
            {!posts?.length ? <Loader size="30px" /> :
                title && <BlockTitle text={title} />}

                {posts.slice(0, limit).map(item => (
                    <Item key={item.id}>
                        <Date>
                            <CalendarIcon color="#7c98f9" />
                            {item.date}
                        </Date>
                        <Name>{item.author.name}</Name>
                        <Body>{item.body}</Body>
                    </Item>
                ))}
        </Container>
    )
}

export default Widget