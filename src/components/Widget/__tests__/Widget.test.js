import "@testing-library/jest-dom"
import {render, waitFor} from "@testing-library/react"

// Component
import Widget from "../Widget"

// Mock Data
import mockData from "./mock-data.json"

describe("Widget component", () => {
    let component

    beforeEach(() => {
        jest.useFakeTimers()
        component = render(<Widget {...mockData} />)
    })

    afterEach(() => {
        jest.runOnlyPendingTimers()
        jest.useRealTimers()
    })

    test("should render loading texts", () => {
        expect(component.container).toHaveTextContent("Loading...")
    })

    test("should render async data", async () => {
        await waitFor(() => {
            expect(component.container).toHaveTextContent("Most Popular")
        })
    })
});