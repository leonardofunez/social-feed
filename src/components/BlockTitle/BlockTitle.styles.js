import styled from "styled-components"

export const Title = styled.h2`
    font-size: 18px;
    border-left: 4px solid red;
    padding-left: 10px;

    @media (min-width: 1000px) {
        font-size: 20px;
    }
`;