// Styles
import {Title} from "./BlockTitle.styles"

const blockTitle = ({text}) => {
    return (
        <Title>{text}</Title>
    )
}

export default blockTitle