import "@testing-library/jest-dom"
import {render} from "@testing-library/react"

// Component
import BlockTitle from "../BlockTitle"

describe("BlockTitle component", () => {
    test("renders content", () => {
        const mockTitle = "Lorem ipsum"
        const component = render(<BlockTitle text={mockTitle} />)
        expect(component.container).toHaveTextContent(mockTitle)
    })
});