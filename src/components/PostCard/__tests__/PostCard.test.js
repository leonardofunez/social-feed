import "@testing-library/jest-dom"
import {render} from "@testing-library/react"

// Component
import PostCard from "../PostCard"

// Mockdata
import mockData from "./mock-data.json"

describe("PostCard component", () => {
    let component

    beforeEach(() => {
        component = render(<PostCard {...mockData} />)
    })

    test("renders props", () => {
        const authorName = component.container.querySelector("h2")
        const authorDescription = component.container.querySelector("p")
        const authorDate = component.container.querySelector("time")
        
        expect(authorName).toHaveTextContent(mockData.author.name)
        expect(authorDescription).toHaveTextContent(mockData.author.description)
        expect(authorDate).toHaveTextContent(mockData.date)
        expect(component.container).toHaveTextContent(mockData.author.followers)
    })

    test("should have defined styles", () => {
        const authorName = component.getByText(mockData.author.name)
        const authorDescription = component.getByText(mockData.author.description)
        const authorBody = component.getByText(mockData.body)
        const postDate = component.getByText(mockData.date)
        expect(authorName).toHaveStyle({"font-size":"18px", "grid-column":"info"})
        expect(authorDescription).toHaveStyle({"font-size":"14px", "grid-column":"info", "overflow":"hidden", "white-space":"nowrap"})
        expect(authorBody).toHaveStyle("line-height: 1.4")
        expect(postDate).toHaveStyle({"font-weight": "600", "display": "flex", "align-items": "center", "margin-right": "20px", "color": "#7c98f9"})
    })
});