// Styles
import {
    Container,
    Date,
    Author,
    AuthorName,
    AuthorDescription,
    AuthorPhotoContainer,
    Followers,
    Location,
    Body,
    Footer
} from "./PostCard.styles"

// Icons
import AvatarIcon from "../../icons/Avatar"
import CalendarIcon from "../../icons/Calendar"
import FollowersIcon from "../../icons/Followers"
import PlaceIcon from "../../icons/Place"

const postCard = ({
    date,
    author,
    body
}) => {
    return (
        <Container>
            <Author>
                <AuthorPhotoContainer>
                    <AvatarIcon size="40" />
                </AuthorPhotoContainer>
                <AuthorName>{author.name}</AuthorName>
                <AuthorDescription>{author.description}</AuthorDescription>
            </Author>
            
            <Body dangerouslySetInnerHTML={{__html: body}} />

            <Footer>
                <Date>
                    <CalendarIcon color="#7c98f9" />
                    {date}
                </Date>
                <Followers>
                    <FollowersIcon size="24" color="#7c98f9" />
                    {author.followers}
                </Followers>
                
                {author.location && (
                    <Location>
                        <PlaceIcon size="14" color="#7c98f9" />
                        {author.location}
                    </Location>
                )}
            </Footer>
        </Container>
    )
}

export default postCard