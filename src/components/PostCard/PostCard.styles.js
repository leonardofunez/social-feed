import styled from "styled-components"

export const Container = styled.article`
    display: grid;
    grid-gap: 20px;
    border-radius: 8px;
    padding: 20px;
    border: 1px solid #0b1e62;
`;

export const Author = styled.div`
    display: grid;
    grid-template-columns: [avatar]40px [info]1fr;
    grid-template-rows: repeat(2, 1fr);
    grid-gap: 6px 20px;
`;

export const AuthorPhotoContainer = styled.div`
    height: 40px;
    width: 40px;
    grid-row: 1 / -1;
    align-self: center;
`;

export const AuthorName = styled.h2`
    font-size: 18px;
    grid-column: info;
`;

export const AuthorDescription = styled.p`
    font-size: 14px;
    grid-column: info;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
`;

export const Body = styled.div`
    line-height: 1.4;
`

export const Footer = styled.footer`
    display: flex;
    align-items: center;
    font-size: 14px;

    > * {
        font-size: 12px;
        font-weight: 600;
        display: flex;
        align-items: center;
        margin-right: 20px;
        color: #7c98f9;
    }

    svg {
        margin-right: 6px;
    }
`;

export const Date = styled.time``;

export const Followers = styled.div``;

export const Location = styled.div``;