import "@testing-library/jest-dom"
import {render, waitFor} from "@testing-library/react"

// Component
import PostCardList from "../PostCardList"

// Mock Data
import mockData from "./mock-data.json"

describe("Widget component", () => {
    let component

    beforeEach(() => {
        jest.useFakeTimers()
        component = render(<PostCardList {...mockData} />)
    })

    afterEach(() => {
        jest.runOnlyPendingTimers()
        jest.useRealTimers()
    })

    test("should render loading texts", () => {
        expect(component.container).toHaveTextContent("Loading feed...")
    })

    test("should render async data", async () => {
        await waitFor(() => {
            expect(component.container).toHaveTextContent("Most Popular")
        })
    })
});