// Hooks
import {useEffect, useState} from "react"

// Styles
import {Container} from "./PostCardList.styles"

// Components
import BlockTitle from "../BlockTitle/BlockTitle"
import PostCard from "../PostCard/PostCard"
import Loader from "../Loader/Loader"

// Serializers
import PostsSerializer from "../../serializers/posts.serializer"

const PostCardList = ({title, limit = 10, updateInt = 5}) => {
    const [posts, setPosts] = useState([])

    const getPosts = async () => {
        try {
            const response = await fetch("https://api.massrelevance.com/MassRelDemo/kindle.json")
            const data = await response.json()
            const posts = PostsSerializer(data)
            
            setPosts(posts)
        } catch (error) {
            console.error("Failed to retrieve data from Massrelevance API");
        }
    }

    useEffect(() => {
        getPosts()

        const updateInterval = setInterval(() => {
            setPosts([])
            getPosts()
        }, updateInt*60000)

        return () => {
            setPosts([])
            clearInterval(updateInterval);
        }
    }, [updateInt])

    return (
        <Container>
            {
                !posts?.length ? 
                    <Loader size="30px" text="Loading feed..." />
                :
                    title && <BlockTitle text={title} />}
                    {posts.slice(0, limit).map(card => (
                        <PostCard {...card} key={card.id}/>
                    ))
            }
        </Container>
    )
}

export default PostCardList