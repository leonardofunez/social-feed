import styled from "styled-components"

export const Container = styled.div`
    display: grid;
    grid-gap: 20px;

    @media screen and (min-width: 1000px) {
        grid-gap: 40px;
    }
`;