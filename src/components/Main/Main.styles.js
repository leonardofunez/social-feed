import styled from "styled-components"

export const Main = styled.main``;

export const MainWrapper = styled.div`
    margin: 0 auto;
    max-width: 980px;
    padding: 40px 10px;
`;