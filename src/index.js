import React from "react"
import ReactDOM from "react-dom"
import MainRoutes from "./config/routes"
import reportWebVitals from './reportWebVitals'
import "./assets/scss/globals.scss"

ReactDOM.render(
	<React.StrictMode>
		{MainRoutes}
	</React.StrictMode>,
	document.getElementById("root")
)

reportWebVitals()