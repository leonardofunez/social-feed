import moment from "moment"

const postSerializer = (cards) => {
    return cards.map((card) => {
        return {
            id: card.id,
            date: moment(new Date(card.created_at)).format("ddd MMM, D"),
            author: {
                name: card.user.name || "",
                description: card.user.description || "",
                followers: card.user.followers_count,
                location: card.user.location
            },
            body: card.text
        }
    })
}

export default postSerializer