import "@testing-library/jest-dom"

// Serializer
import postSerializer from "../posts.serializer"

describe("Posts Serializer", () => {
    test("should returns the right serialized data", () => {
        const mockData = [
            {
                "created_at": "Fri Dec 29 19:15:04 +0000 2017",
                "id": 946821889648980000,
                "user": {
                    "location": "Kansas",
                    "name": "Lori King",
                    "description": "Dirtyminded girl with a romantic twist. #BestSelling #Author of #eroticromance. I'm seducing the world, one book at a time. #supermom #rockstar #spaz",
                    "followers_count": 2977
                },
                "text": "The battle is life or death, but that doesn’t mean love plays no part. #shifters #menage #paranormal #ebook… https://t.co/61xUthkXFN"
            }
        ]
        
        const serializedData = [
            {
                "id": 946821889648980000,
                "date": "Fri Dec, 29",
                "author":
                    {
                        "name": "Lori King",
                        "description": "Dirtyminded girl with a romantic twist. #BestSelling #Author of #eroticromance. I'm seducing the world, one book at a time. #supermom #rockstar #spaz",
                        "followers": 2977,
                        "location": "Kansas"
                    },
                "body": "The battle is life or death, but that doesn’t mean love plays no part. #shifters #menage #paranormal #ebook… https://t.co/61xUthkXFN"
            }
        ]

        const shapedData = postSerializer(mockData)
        expect(shapedData).toEqual(serializedData)
    })
});