// Components
import BlockTitle from "../../components/BlockTitle/BlockTitle"

const ErrorPage = () => {
    return (
        <BlockTitle text="Error Page" />
    )
}

export default ErrorPage