import "@testing-library/jest-dom"
import {render} from "@testing-library/react"

// Page
import ErrorPage from "../ErrorPage"

describe("Error page", () => {
    test("should render error page information", () => {
        const page = render(<ErrorPage />)
        expect(page.container).toHaveTextContent("Error Page")
    })
})