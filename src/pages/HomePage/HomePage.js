// Styles
import {Container, Aside} from "./HomePage.styles"

// Components
import PostCardList from "../../components/PostCardList/PostCardList"
import Widget from "../../components/Widget/Widget"

const HomePage = () => {
    return (
        <Container>
            <PostCardList title="Social Feed" limit={8} updateInt={2} />
            
            <Aside>
                <Widget title="Most Popular" limit={4} updateInt={1} />
            </Aside>
        </Container>
    )
}

export default HomePage