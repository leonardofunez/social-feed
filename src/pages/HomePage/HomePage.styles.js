import styled from "styled-components"

export const Container = styled.div`
    display: grid;
    grid-template-columns: 1fr;
    grid-gap: 40px;
    position: relative;

    @media (min-width: 1000px) {
        grid-template-columns: 1fr 300px;
    }
`;

export const Aside = styled.aside``;