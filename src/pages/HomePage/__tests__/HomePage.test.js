import "@testing-library/jest-dom"
import {render} from "@testing-library/react"

// Page
import HomePage from "../HomePage"

describe("Error page", () => {
    let homePage

    beforeEach(() => {
        homePage = render(<HomePage />)
    })
    
    test("should render loading texts", () => {
        expect(homePage.container).toHaveTextContent("Loading feed...")
        expect(homePage.container).toHaveTextContent("Loading...")
    })
})