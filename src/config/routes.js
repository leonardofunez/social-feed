import { BrowserRouter, Routes, Route } from "react-router-dom"

// Pages
import HomePage from "../pages/HomePage/HomePage"
import ErrorPage from "../pages/ErrorPage/ErrorPage"

// Root Styles
import { Main, MainWrapper } from "../components/Main/Main.styles"

const MainRoutes = (
    <BrowserRouter>
        <Main>
            <MainWrapper>
                <Routes>
                    <Route element={<HomePage />} path="/" exact />
                    <Route element={<ErrorPage />} path="*"/>
                </Routes>
            </MainWrapper>
        </Main>
    </BrowserRouter>
)

export default MainRoutes